﻿using System.IO;
using System.Threading.Tasks;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

namespace Docitt.Document.Controller.Api.ActionResults
{
    internal class FileActionResult : ActionResult
    {
        public FileActionResult(string fileDownloadName, byte[] content, string contentType = null)
        {
            FileDownloadName = fileDownloadName;
            Content = content;
            ContentType = contentType ?? MimeMapping.GetMimeMapping(Path.GetExtension(fileDownloadName));
        }

        public string ContentType { get; }
        public string FileDownloadName { get; }
        public byte[] Content { get; }

        public override async Task ExecuteResultAsync(ActionContext context)
        {
            var response = context.HttpContext.Response;
            response.ContentType = ContentType;
            response.ContentLength = Content.Length;
            response.Headers.Add("Content-Disposition", new[] { "attachment; filename=" + FileDownloadName });

            using (var stream = new MemoryStream(Content))
            {
                await stream.CopyToAsync(context.HttpContext.Response.Body);
            }
        }
    }
}
