﻿using System.Collections.Generic;

namespace Docitt.Document.Controller.Api.ActionResults
{
    internal class MimeMapping
    {
        private static readonly Dictionary<string, string> ExtensionMap = new Dictionary<string, string>();

        static MimeMapping()
        {
            ExtensionMap.Add(".zip", "application/x-zip-compressed");
            ExtensionMap.Add(".*", "application/octet-stream");
        }

        public static string GetMimeMapping(string fileExtension)
        {
            if (ExtensionMap.ContainsKey(fileExtension.ToLowerInvariant()))
                return ExtensionMap[fileExtension];
            return ExtensionMap[".*"];
        }
    }
}
