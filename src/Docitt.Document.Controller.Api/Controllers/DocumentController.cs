﻿
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Threading.Tasks;
using System;

using Docitt.Document.Controller.Api.ActionResults;

namespace Docitt.Document.Controller.Api.Controllers
{
    /// <summary>
    /// Controller
    /// </summary>
    [Route("/")]
    public class DocumentController : ExtendedController
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="documentController"></param>
        /// <exception cref="ArgumentException"></exception>
        public DocumentController(IDocumentService documentController)
        {
            DocumentService = documentController;
            if (DocumentService == null)
                throw new ArgumentException($"{nameof(DocumentService)} is mandatory");
        }

        private IDocumentService DocumentService { get; }

        /// <summary>
        /// DownloadAllDocuments
        /// </summary>
        /// <param name="entityType">entityType</param>
        /// <param name="entityId">entityId</param>
        /// <returns>FileActionResult</returns>
        [HttpGet("{entityType}/{entityId}/download/all")]
#if DOTNET2
        [ProducesResponseType(typeof(FileActionResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> DownloadAllDocuments(string entityType,string entityId)
        {
            try
            {
                var zipFileResponse = await DocumentService.DownloadDocuments(entityType, entityId,false);
                return new FileActionResult(zipFileResponse.ZipFileName, zipFileResponse.ZipFileData);
            }
            catch (ArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }            
            catch (InvalidOperationException)
            {
                return ErrorResult.BadRequest("Cannot generate zip file for the documents");
            }
        }

        /// <summary>
        /// DownloadDocumentsByUser
        /// </summary>
        /// <param name="entityType">entityType</param>
        /// <param name="entityId">entityId</param>
        /// <returns>FileActionResult</returns>
        [HttpGet("{entityType}/{entityId}/download/mine")]
#if DOTNET2
        [ProducesResponseType(typeof(FileActionResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> DownloadDocumentsByUser(string entityType, string entityId)
        {
            try
            {
                var zipFileResponse = await DocumentService.DownloadDocuments(entityType, entityId, true);
                return new FileActionResult(zipFileResponse.ZipFileName, zipFileResponse.ZipFileData);
            }
            catch (ArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (InvalidOperationException)
            {
                return ErrorResult.BadRequest("Cannot generate zip file for the documents");
            }
        }

        /// <summary>
        /// GetDocumentsByUser
        /// </summary>
        /// <param name="entityType">entityType</param>
        /// <param name="entityId">entityId</param>
        /// <returns>FileActionResult</returns>
        [HttpGet("{entityType}/{entityId}/mine")]
#if DOTNET2
        [ProducesResponseType(typeof(FileActionResult), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetDocumentsByUser(string entityType, string entityId)
        {
            try
            {   
                return await ExecuteAsync(async () =>
                {
                    var result = await DocumentService.GetDocuments(entityType, entityId, true);

                    return Ok(result);
                });
            }
            catch (ArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }
            catch (InvalidOperationException)
            {
                return ErrorResult.BadRequest("Cannot get document list.");
            }
        }
    }
}
