﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace Docitt.Document.Controller.Client
{
    public static class DocumentServiceClientExtensions
    {
         [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddApplicantDocumentService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IDocumentServiceClientFactory>(p => new DocumentServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IDocumentServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddApplicantDocumentService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IDocumentServiceClientFactory>(p => new DocumentServiceClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IDocumentServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddApplicantDocumentService(this IServiceCollection services)
        {
            services.AddTransient<IDocumentServiceClientFactory>(p => new DocumentServiceClientFactory(p));
            services.AddTransient(p => p.GetService<IDocumentServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
