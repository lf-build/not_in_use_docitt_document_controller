﻿using System;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;

namespace Docitt.Document.Controller.Client
{
    public class DocumentServiceClientFactory : IDocumentServiceClientFactory
    {      
        [Obsolete("Need to use the overloaded with Uri")]
        public DocumentServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public DocumentServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; set; }
        private Uri Uri { get; }

        public IDocumentService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("document_controller");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new DocumentServiceClient(client);
        }
    }
}
