﻿
using LendFoundry.Security.Tokens;

namespace Docitt.Document.Controller.Client
{
    public interface IDocumentServiceClientFactory
    {
        IDocumentService Create(ITokenReader reader);
    }
}
