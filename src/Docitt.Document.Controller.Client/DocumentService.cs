﻿using LendFoundry.Foundation.Client;
using System.Threading.Tasks;
using System;
using LendFoundry.DocumentManager;
using System.Collections.Generic;

namespace Docitt.Document.Controller.Client
{
    public class DocumentServiceClient : IDocumentService
    {
        public DocumentServiceClient(IServiceClient client)
        {
            Client = client;
        }
        private IServiceClient Client { get; }

        public async Task<IDownloadZipResponse> DownloadDocuments(string entityType, string entityId, bool isUserSpecific)
        {
            if (!isUserSpecific)
                return await Client.GetAsync<DownloadZipResponse>($"{entityType}/{entityId}/download/all");
            else
                return await Client.GetAsync<DownloadZipResponse>($"{entityType}/{entityId}/download/mine");
        }

        public async Task<IEnumerable<IDocument>> GetDocuments(string entityType, string entityId, bool isUserSpecific)
        {
            return await Client.GetAsync<List<LendFoundry.DocumentManager.Document>>($"{entityType}/{entityId}/download/mine");
        }
    }
}
