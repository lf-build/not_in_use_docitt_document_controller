﻿namespace Docitt.Document.Controller
{
    public class DownloadZipResponse :IDownloadZipResponse
    {
       public string ZipFileName { get; set; }
       public byte[] ZipFileData { get; set; }
    }
}
