﻿
using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace Docitt.Document.Controller
{
    public class Configuration : IDependencyConfiguration
    {
        public string  DownloadZipFileName { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
