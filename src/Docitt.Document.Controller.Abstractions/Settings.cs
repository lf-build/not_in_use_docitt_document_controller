﻿using System;

namespace Docitt.Document.Controller
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "document-controller";
    }
}
