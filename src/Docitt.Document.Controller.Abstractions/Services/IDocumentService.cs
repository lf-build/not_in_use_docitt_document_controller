﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.DocumentManager;
using System.IO;

namespace Docitt.Document.Controller
{
    public interface IDocumentService
    {
        Task<IDownloadZipResponse> DownloadDocuments(string entityType,string entityId,bool isUserSpecific);

        Task<IEnumerable<IDocument>> GetDocuments(string entityType, string entityId, bool isUserSpecific);
    }
}
