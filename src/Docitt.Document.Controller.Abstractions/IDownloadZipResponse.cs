﻿namespace Docitt.Document.Controller
{
    public interface IDownloadZipResponse
    {
        byte[] ZipFileData { get; set; }
        string ZipFileName { get; set; }
    }
}