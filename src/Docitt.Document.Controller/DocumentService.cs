﻿using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Security.Tokens;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Document.Controller
{
    public class DocumentService : IDocumentService
    {
        #region Constructor

        public DocumentService(IDocumentManagerService documentManagerService,
            Configuration config,
            ITokenReader tokenReader,
            ITokenHandler tokenParser,
            ILookupService lookup)
        {
            if (documentManagerService == null) throw new ArgumentException($"{nameof(documentManagerService)} is mandatory");
            if (config == null) throw new ArgumentException($"{nameof(config)} is mandatory");
            if (tokenReader == null) throw new ArgumentException($"{nameof(tokenReader)} is mandatory");
            if (tokenParser == null) throw new ArgumentException($"{nameof(tokenParser)} is mandatory");
            if (lookup == null) throw new ArgumentException($"{nameof(lookup)} is mandatory");


            DocumentManager = documentManagerService;
            Configuration = config;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
            Lookup = lookup;
        }

        #endregion

        #region Private Variables

        private Configuration Configuration { get; }
        private IDocumentManagerService DocumentManager { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenParser { get; }

        private ILookupService Lookup { get; }
        #endregion
        /// <summary>
        /// Download all or user specific document as a zip for supplied entityId and entity type.
        /// </summary>
        /// <param name="entityType">The entity type. ex application, user etc....</param>
        /// <param name="entityId">The entity id. Ex : application numbner , userName etc...</param>
        /// <param name="isUserSpecific">bool value for user specific</param>
        /// <returns>Download zip response object.</returns>
        public async Task<IDownloadZipResponse> DownloadDocuments(string entityType, string entityId, bool isUserSpecific)
        {
            IEnumerable<IDocument> documentList;
            if (!isUserSpecific)
            {
                documentList = await GetDocumentList(entityType, entityId, string.Empty);
            }
            else
            {
                var userName = GetTokenUserName();

                if (string.IsNullOrWhiteSpace(userName))
                    throw new ArgumentException("Token user cannot be found in the request, please verify");

                //fetch the user name from header
                documentList = await GetDocumentList(entityType, entityId, userName);
            }

            using (var outStream = new MemoryStream())
            {
                using (var archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
                {
                    foreach (var document in documentList)
                    {
                        //download the document from document manager
                        var documentContent = await DocumentManager.Download(entityType, entityId, document.Id);
                        var fileInArchive = archive.CreateEntry(document.Id + "_" + document.FileName);
                        using (var entryStream = fileInArchive.Open())
                        using (var fileToCompressStream = new MemoryStream(ConvertStreamToBytes(documentContent)))
                        {
                            fileToCompressStream.CopyTo(entryStream);
                        }
                    }
                }
                var downloadZipResponse = new DownloadZipResponse();
                downloadZipResponse.ZipFileName = Configuration.DownloadZipFileName;
                downloadZipResponse.ZipFileData = outStream.ToArray();
                return downloadZipResponse;
            }
        }

        /// <summary>
        /// Download all or user specific document list for supplied entityId and entity type.
        /// </summary>
        /// <param name="entityType">The entity type. ex application, user etc....</param>
        /// <param name="entityId">The entity id. Ex : application numbner , userName etc...</param>
        /// <param name="isUserSpecific">bool value for user specific</param>
        /// <returns>List of document object.</returns>
        public async Task<IEnumerable<IDocument>> GetDocuments(string entityType, string entityId, bool isUserSpecific)
        {
            if (!isUserSpecific)
            {
                return await GetDocumentList(entityType, entityId, string.Empty);
            }
            else
            {
                var userName = GetTokenUserName();

                if (string.IsNullOrWhiteSpace(userName))
                    throw new ArgumentException("Token user cannot be found in the request, please verify");

                //fetch the user name from header
                return await GetDocumentList(entityType, entityId, userName);
            }

        }
        private string GetTokenUserName()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            return token.Subject;
        }

        /// <summary>
        /// Download all or user specific document list for supplied entityId and entity type.
        /// </summary>
        /// <param name="entityType">The entity type. ex application, user etc....</param>
        /// <param name="entityId">The entity id. Ex : application numbner , userName etc...</param>
        /// <param name="userName">User name</param>
        /// <returns>List of document object.</returns>
        private async Task<IEnumerable<IDocument>> GetDocumentList(string entityType, string entityId, string userName)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException("Argument is null or whitespace", nameof(entityType));

            EnsureEntityTypeIsValid(entityType);
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException("Argument is null or whitespace", nameof(entityId));

            var docList = (await DocumentManager.GetAll(entityType, entityId)).ToList();

            if (!string.IsNullOrEmpty(userName))
            {
                List<IDocument> documentList = new List<IDocument>();
                string createdBy = string.Empty;
                foreach (var doc in docList)
                {
                    if (doc.Metadata != null)
                    {
                        var metaData = JObject.Parse(doc.Metadata.ToString());
                        if (metaData != null)
                        {
                            if (metaData["createdBy"] != null)
                            {
                                createdBy = metaData["createdBy"].ToString();
                                if (createdBy == userName)
                                    documentList.Add(doc);
                            }
                        }
                    }
                }
                return documentList;
            }
            return docList;
        }
        private static byte[] ConvertStreamToBytes(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        private string EnsureEntityTypeIsValid(string entityType)
        {
            entityType = entityType.ToLower();
            if (Lookup.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");
            return entityType;
        }
    }
}
