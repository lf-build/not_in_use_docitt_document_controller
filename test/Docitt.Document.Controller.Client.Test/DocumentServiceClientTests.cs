﻿using LendFoundry.DocumentManager;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Docitt.Document.Controller.Client.Test
{
    public class DocumentServiceClientTests
    {
        private string EntityType { get; set; } = "application";
        private IDocumentService DocumentServiceClient { get; }
        private IRestRequest request { get; set; }

        private Mock<IServiceClient> MockServiceClient { get; }
        
     
        public DocumentServiceClientTests()
        {
            //Request = new RestRequest();
            MockServiceClient = new Mock<IServiceClient>();
            DocumentServiceClient = new DocumentServiceClient(MockServiceClient.Object);
        }

    

        [Fact]
        public void Client_DownloadDocumentsAll()
        {
            IRestRequest request = null;        

            MockServiceClient.Setup(s => s.ExecuteAsync<DownloadZipResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .ReturnsAsync(new DownloadZipResponse());

            var result = DocumentServiceClient.DownloadDocuments("application","000001",false).Result;

            //Assert.Equal(expected, result);
            Assert.Equal("{entityType}/{entityId}/download/all", request.Resource);
            Assert.Equal(Method.GET, request.Method);
        }

        [Fact]
        public void Client_DownloadDocumentsByUser()
        {
            IRestRequest request = null;
            MockServiceClient.Setup(s => s.ExecuteAsync<DownloadZipResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .ReturnsAsync(new DownloadZipResponse());

            var result = DocumentServiceClient.DownloadDocuments("application", "000001",true).Result;

            //Assert.Equal(expected, result);
            Assert.Equal("{entityType}/{entityId}/download/mine", request.Resource);
            Assert.Equal(Method.GET, request.Method);
        }

      

       
        [Fact]
        public void GetAllDocuments_List()
        {
            var expected = new List<LendFoundry.DocumentManager.Document>();

            MockServiceClient.Setup(s => s.ExecuteAsync<List<LendFoundry.DocumentManager.Document>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .ReturnsAsync(expected);

            var result = DocumentServiceClient.GetDocuments("loans", "test",false).Result;
            Assert.NotNull(result);
            Assert.Equal(result.ToList().Count, 0);
        }


    }
}
