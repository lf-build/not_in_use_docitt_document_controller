﻿using Moq;
using Microsoft.AspNet.Http;
using System.IO;
using Xunit;
using Docitt.Document.Controller.Api.Controllers;
using LendFoundry.DocumentManager;
using Docitt.Document.Controller.Api.ActionResults;
using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Lookup;
using Microsoft.AspNet.Mvc;

namespace Docitt.Document.Controller.Api.Tests
{
    public class DocumentControllerTest
    {
        Mock<IDocumentService>  documentService;
        Mock<ITokenReader> tokenReader;
        Mock<ITokenHandler> tokenHandler;
        Mock<ILookupService> LookUp;

        DocumentController Api;
        public DocumentControllerTest()
        {
            documentService = new Mock<IDocumentService>();
            tokenReader = new Mock<ITokenReader>();
            tokenHandler = new Mock<ITokenHandler>();
            LookUp = new Mock<ILookupService>();
            Api = new DocumentController(documentService.Object);
        }

        private IDocument CurrentDocument { get; set; }

        private DocumentController GetApplicationDocumentController
        {
            get
            {
                var documentManagerService = new Mock<IDocumentManagerService>();

                IDocumentService applicationdocumentService = new DocumentService(
                    documentManagerService.Object, Mock.Of<Configuration>(), tokenReader.Object,tokenHandler.Object, LookUp.Object);

                return new DocumentController(applicationdocumentService);
            }
        }

        [Fact]
        public void Controller_With_Null_Services_Should_Throw_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => new DocumentController(null));
        }
        
    }
}
